# RISC-V Assembly Program template
This repository contains an example configuration of a RV32I assembly program executing on the A-Core processor.

Note that this repository is not usable as-is, you must use it as part of `riscv-assembly-tests` repository which provides the default Makefile and linker script. Naturally, you may also write your own Makefile and/or linker script specific for this software program. You can also include the default Makefile and override some symbols, such as `MARCH` and `MABI`.

## Repository Structure
* README.md -- you are reading it right now
* Makefile -- build system configuration for GNU make. Includes default makefile and overrides `MARCH` and `MABI` symbols..
* a-core.ld -- [linker script](https://sourceware.org/binutils/docs/ld/Scripts.html) for the A-Core target. Includes default linker script.
* main.s -- Main assembly program source

## Linking
The linking process is controlled by the linker script `a-core.ld` which contains configurations for binaries
targeting A-Core. For reference, the default RISC-V linker script can be viewed with `riscv64-unknown-elf-ld --verbose`.

## Compilation Instructions
The program can be built with `make` which produces the binary file ending with `.elf`.
